
public class Aufgabe3 {

	public static void main(String[] args) {
		System.out.printf("\n%-12s%s%10s", "Fahrenheit", "|", "Celsius");
		System.out.printf("\n%s", "------------------------");
		System.out.printf("\n%-12s%s%10s", "-20", "|", "-28.89");
		System.out.printf("\n%-12s%s%10s", "-10", "|", "-23.33");
		System.out.printf("\n%-12s%s%10s", "+0", "|", "-17.78");
		System.out.printf("\n%-12s%s%10s", "+20", "|", "-6.67");
		System.out.printf("\n%-12s%s%10s", "+30", "|", "-1.11");
	}

}
