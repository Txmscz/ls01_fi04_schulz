﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
        while(true) {
    	double gesamtBetrag = fahrkartenbestellungErfassen();
        double rückgabebetrag = fahrkartenBezahlen(gesamtBetrag);
        fahrkartenAusgeben();
        rueckgeldAusgeben(rückgabebetrag);
        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                "vor Fahrtantritt entwerten zu lassen!\n"+
                "Wir wünschen Ihnen eine gute Fahrt.");
        System.out.println("\n");
        }
    }
    public static double fahrkartenbestellungErfassen(){
    	Scanner tastatur = new Scanner(System.in);
    	
    	double anzahlTickets = 0;
    	double zuZahlenderBetrag; 
    	double gesamtBetrag = 0;
    	int eingabe;
    	double zwischenPreis;
    	double[] betrag = new double[10];
    	String[] fahrkarten = new String[10];
    	
    	betrag[0] = 2.9;
    	betrag[1] = 3.3;
    	betrag[2] = 3.6;
    	betrag[3] = 1.9;
    	betrag[4] = 8.6;
    	betrag[5] = 9.0;
    	betrag[6] = 9.6;
    	betrag[7] = 23.5;
    	betrag[8] = 24.3;
    	betrag[9] = 24.9;
    	
    	fahrkarten[0] = "Einzelfahrschein Berlin AB";
    	fahrkarten[1] = "Einzelfahrschein Berlin BC";
    	fahrkarten[2] = "Einzelfahrschein Berlin ABC";
    	fahrkarten[3] = "Kurzstrecke";
    	fahrkarten[4] = "Tageskarte Berlin AB";
    	fahrkarten[5] = "Tageskarte Berlin BC";
    	fahrkarten[6] = "Tageskarte Berlin ABC";
    	fahrkarten[7] = "Kleingruppen-Tageskarte Berlin AB";
    	fahrkarten[8] = "Kleingruppen-Tageskarte Berlin BC";
    	fahrkarten[9] = "Kleingruppen-Tageskarte Berlin ABC";
    	

    	
    	do {
    		System.out.printf("%2s%39s\n", 0, "Bezahlen");
    		for(int index = 0; index < fahrkarten.length; index++) {
    			System.out.printf("%2s%39s%10s\n",index + 1,fahrkarten[index], betrag[index]);
    	}
    	
    		System.out.println("Wählen sie ihre gewünschte Fahrkarte aus:");
    		eingabe = tastatur.nextInt();
    		eingabe -= 1;
    		if (eingabe >= 0 && eingabe < 11) {
    			zuZahlenderBetrag = betrag[eingabe];
    			System.out.println("Anzahl der Tickets: ");
    			anzahlTickets = tastatur.nextDouble();
    		
    		if (anzahlTickets >= 1 && anzahlTickets <= 10) {
    			zwischenPreis = zuZahlenderBetrag * anzahlTickets;
    			gesamtBetrag += zwischenPreis;
    			System.out.println(gesamtBetrag);
    		}
    		else {
    			System.out.print("Sie können nicht weniger als 1 Ticket bestllen und auch nicht mehr als 10 zur selben Zeit.\n");
    			System.out.print("Sie können nur wenigstens 1 und maximal 10 Tickets zur selben Zeit bestellen!\n");
    		}  
    	}
    	}while(eingabe !=-1);
        return gesamtBetrag;
    }
    public static double fahrkartenBezahlen(double gesamtBetrag){
        double eingezahlterGesamtbetrag;
        double eingeworfeneMünze;
        double rückgabebetrag;
        Scanner tastatur = new Scanner(System.in);
        
        // Geldeinwurf
        // -----------
        eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < gesamtBetrag)
        {
     	   System.out.printf("Noch zu zahlen: %.2f Euro", (gesamtBetrag - eingezahlterGesamtbetrag));
     	   System.out.print("\nEingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
        rückgabebetrag = eingezahlterGesamtbetrag - gesamtBetrag;
        System.out.print(rückgabebetrag);
		return rückgabebetrag;
    }
    public static void fahrkartenAusgeben() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben(double rückgabebetrag) {
         
         if(rückgabebetrag > 0.0)
         {
      	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro", rückgabebetrag);
      	   System.out.println("wird in folgenden Münzen ausgezahlt:");

             while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
             {
          	  System.out.println("2 EURO");
  	          rückgabebetrag -= 2.0;
             }
             while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
             {
          	  System.out.println("1 EURO");
  	          rückgabebetrag -= 1.0;
             }
             while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
             {
          	  System.out.println("50 CENT");
  	          rückgabebetrag -= 0.5;
             }
             while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
             {
          	  System.out.println("20 CENT");
   	          rückgabebetrag -= 0.2;
             }
             while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
             {
          	  System.out.println("10 CENT");
  	          rückgabebetrag -= 0.1;
             }
             while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
             {
          	  System.out.println("5 CENT");
   	          rückgabebetrag -= 0.05;
             }
         }

    }
    //public static double Fahrkartenmenue() {
    	//Scanner tastatur = new Scanner(System.in);
    	//double zuZahlenderBetrag;
//    	int nutzerwahl = tastatur.nextInt();
  //  	switch(nutzerwahl) {
    //	case 1: zuZahlenderBetrag = 3.6;
    //		break;
    //	case 2: zuZahlenderBetrag = 7.2;
    //		break;
    //	case 3: zuZahlenderBetrag = 22.0;
    //		break;
    //	default: System.out.println("Sie haben eine Fehleingabe getätigt und bekommen deswegen einen Einzellfahrschein.");
    //		zuZahlenderBetrag = 3.6;
    //	}
    	//return zwischenPreis;
}

